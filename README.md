# Zeplinception

Zeplin Challenge - [Demo Page](http://zeplinception.surge.sh/)

# Built with

* [Create React App](https://github.com/facebook/create-react-app)
* [React.js](https://facebook.github.io/react/)
* [Redux](http://redux.js.org/)
* [Styled Components](https://www.styled-components.com/)
* [Jest](https://facebook.github.io/jest/)

# Run

```bash
$ git clone https://gitlab.com/mertkahyaoglu/zeplin-challenge.git
$ cd zeplin-challenge
$ yarn
$ yarn start
```

# Todos

* Image Loading
* Enzyme tests
* Caching (on network or redux layer)
* Service worker

## License

MIT © [Mert Kahyaoğlu](http://mert-kahyaoglu.com/) 2018
