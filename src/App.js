import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Main from './layouts/Main';

const App = () => {
  return (
    <Main>
      <Switch>
        <Route path="/" component={Main} />
      </Switch>
    </Main>
  );
};

export default App;
