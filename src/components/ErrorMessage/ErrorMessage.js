import React from 'react';
import styled from 'styled-components';

import ErrorIcon from 'assets/error.svg';

const ErrorMessage = ({ error }) => {
  return (
    <Container>
      <Icon src={ErrorIcon} alt="zeplinception" />
      <Error>{error}</Error>
    </Container>
  );
};

export default ErrorMessage;

const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex: 1;
  flex-direction: column;
`;

const Icon = styled.img`
  width: 200px;
`;

const Error = styled.h1`
  color: ${props => props.theme.dangerColor};
  font-size: 28px;
  font-weight: 500;
`;
