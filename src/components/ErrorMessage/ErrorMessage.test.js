import React from 'react';
import renderer from 'react-test-renderer';

import ErrorMessage from './ErrorMessage';

describe('component/ErrorMessage', () => {
  it('should create the component', () => {
    const component = renderer.create(
      <ErrorMessage error="Network error occured" />
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
