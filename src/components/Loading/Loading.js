import React from 'react';
import styled from 'styled-components';

import LoadingIcon from 'assets/loading.svg';

const Loading = () => {
  return (
    <Container>
      <img src={LoadingIcon} alt="zeplinception" />
    </Container>
  );
};

export default Loading;

const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex: 1;
`;
