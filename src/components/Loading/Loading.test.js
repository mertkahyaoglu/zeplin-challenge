import React from 'react';
import renderer from 'react-test-renderer';

import Loading from './Loading';

describe('component/Loading', () => {
  it('should create the component', () => {
    const component = renderer.create(<Loading />);
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
