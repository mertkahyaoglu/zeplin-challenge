import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const Member = ({
  index,
  member: { role, user: { username, email, avatar } },
}) => {
  return (
    <Container isOdd={index % 2 === 1}>
      <Avatar src={avatar} />
      <MemberInfoWrapper>
        <Username>{username}</Username>
        <Role>{role}</Role>
      </MemberInfoWrapper>
    </Container>
  );
};

Member.propTypes = {
  index: PropTypes.number.isRequired,
  member: PropTypes.object.isRequired,
};

export default Member;

const Container = styled.div`
  display: flex;
  padding: 12px;
  background-color: ${props =>
    props.isOdd ? props.theme.sidebarColor : 'white'};
`;

const Avatar = styled.img`
  border-radius: 50%;
  width: 60px;
  height: 60px;
`;

const MemberInfoWrapper = styled.div`
  padding: 12px;
`;

const Username = styled.div``;

const Role = styled.div`
  color: ${props => props.theme.lightTextColor};
`;
