import React from 'react';
import renderer from 'react-test-renderer';

import Member from './Member';

const member = {
  role: 'owner',
  user: {
    username: 'berk',
    email: 'berk@zeplin.io',
    avatar: 'https://www.gravatar.com/avatar/51b3b8f583a4a342499ecf3d702baf89',
  },
};

describe('component/Member', () => {
  it('should create the component', () => {
    const component = renderer.create(<Member index={1} member={member} />);
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
