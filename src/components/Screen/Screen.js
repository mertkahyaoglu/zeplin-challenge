import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import { getAssetUrl } from 'utils/helpers';

import ErrorIcon from 'assets/error.svg';

export default class Screen extends Component {
  state = {
    loadFailed: false,
  };

  handleError = () => {
    this.setState({ loadFailed: true });
  };

  render() {
    const { loadFailed } = this.state;
    const { name, thumbnail, onDoubleClick } = this.props;
    return (
      <Container onDoubleClick={onDoubleClick}>
        {loadFailed ? (
          <Image src={ErrorIcon} alt={name} />
        ) : (
          <Image
            src={getAssetUrl(thumbnail.d1x)}
            srcset={getAssetUrl(thumbnail.d2x)}
            onError={this.handleError}
            alt={name}
          />
        )}
        <ScreenName>{name}</ScreenName>
      </Container>
    );
  }
}

Screen.propTypes = {
  name: PropTypes.string.isRequired,
  thumbnail: PropTypes.object.isRequired,
  onDoubleClick: PropTypes.func.isRequired,
};

const Container = styled.div``;

const Image = styled.img`
  height: 180px;
  width: 220px;
  border: 1px solid ${props => props.theme.primaryColor};
  &:hover {
    border-color: ${props => props.theme.secondaryColor};
  }
`;

const ScreenName = styled.h3`
  width: 220px;
  margin-top: 6px;
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
`;
