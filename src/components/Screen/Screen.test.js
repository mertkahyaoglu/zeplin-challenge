import React from 'react';
import renderer from 'react-test-renderer';

import Screen from './Screen';

const screen = {
  _id: '1',
  name: 'Album',
  tags: ['Music', 'Lock Screen'],
  thumbnail: {
    d1x: '/static/13/screen-thumbnails/1.png',
    d2x: '/static/13/screen-thumbnails/1@2x.png',
  },
  width: 750,
  height: 1334,
  imageUrl: '/static/13/screen-images/1.png',
};

describe('component/Screen', () => {
  it('should create the component', () => {
    const component = renderer.create(
      <Screen
        name={screen.name}
        thumbnail={screen.thumbnail}
        onDoubleClick={f => f}
      />
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
