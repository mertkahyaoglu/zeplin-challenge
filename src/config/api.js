export const BASE_URL = 'https://zpl-mix.now.sh';

export const getProjectUrl = pid => `${BASE_URL}/projects/${pid}`;
export const getProjectUsersUrl = pid => `${BASE_URL}/projects/${pid}/users`;
export const getProjectScreenUrl = (pid, sid) =>
  `${BASE_URL}/projects/${pid}/screens/${sid}`;
