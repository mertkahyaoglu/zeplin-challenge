import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { filterScreen, removeFilter } from 'ducks/filters';

export class Filters extends Component {
  handleTagClick = tag => {
    const { filterScreen, removeFilter, selectedTag } = this.props;
    if (tag === selectedTag) {
      removeFilter();
    } else {
      filterScreen(tag);
    }
  };

  render() {
    const { tags, selectedTag } = this.props;
    return (
      <Container>
        {tags.map(tag => (
          <Tag
            key={tag}
            active={selectedTag === tag}
            onClick={() => this.handleTagClick(tag)}
          >
            {tag}
          </Tag>
        ))}
      </Container>
    );
  }
}

Filters.propTypes = {
  tags: PropTypes.array.isRequired,
  filterScreen: PropTypes.func.isRequired,
  removeFilter: PropTypes.func.isRequired,
  selectedTag: PropTypes.string,
};

const mapStateToProps = ({ filters: { selectedTag } }) => ({
  selectedTag,
});

const mapDispatchToProps = dispatch => ({
  filterScreen: tag => dispatch(filterScreen(tag)),
  removeFilter: () => dispatch(removeFilter()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Filters);

const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
  background-color: white;
  padding: 12px 6px 0 6px;
  position: sticky;
  top: 0;
`;

const Tag = styled.span`
  height: 24px;
  line-height: 23px;
  padding: 0 12px;
  border-radius: 12px;
  background-color: ${props =>
    props.active ? props.theme.darkGray : props.theme.gray};
  color: ${props => props.theme.textColor};
  text-align: center;
  margin: 12px;
  cursor: pointer;
  user-select: none;
`;
