import React from 'react';
import renderer from 'react-test-renderer';
import configureStore from 'redux-mock-store';

import Filters from './Filters';

const mockStore = configureStore([]);

const store = mockStore({
  filters: {
    selectedTag: 'Album',
  },
});

const tags = ['Album', 'Music', 'Notification'];

describe('containers/Filters', () => {
  it('should render the container', () => {
    const item = renderer
      .create(<Filters tags={tags} store={store} />)
      .toJSON();

    expect(item).toMatchSnapshot();
  });
});
