import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import logo from 'assets/logo-zeplin.svg';
import notificationIcon from 'assets/ic-notification.svg';

import { media } from 'utils/helpers';

const Header = ({ title, prevRoute, prevTitle }) => {
  return (
    <Container>
      <HeaderLeft>
        <Logo src={logo} alt="Zeplin" />
        {prevRoute && (
          <StyledLink to={prevRoute}>
            <LeftArrow />
            {prevTitle}
          </StyledLink>
        )}
      </HeaderLeft>
      <Title>{title}</Title>
      <HeaderRight>
        <NotificationButton
          onClick={f => f}
          src={notificationIcon}
          alt="Zeplin"
        />
      </HeaderRight>
    </Container>
  );
};

Header.propTypes = {
  title: PropTypes.string,
  prevTitle: PropTypes.string,
  prevRoute: PropTypes.string,
};

const mapStateToProps = ({ layout: { title, prevRoute, prevTitle } }) => ({
  title,
  prevRoute,
  prevTitle,
});

export default connect(mapStateToProps)(Header);

const Container = styled.header`
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  height: ${props => props.theme.headerHeight}px;
  padding: 0 12px;
  background-color: #ffffff;
  box-shadow: 0 2px 4px 0 rgba(87, 71, 81, 0.2);
  z-index: 100;
`;

const Title = styled.h2`
  color: ${props => props.theme.textColor};
  width: 480px;
  white-space: nowrap;
  text-overflow: ellipsis;
  text-align: center;
  overflow: hidden;
  ${media.md`
    width: auto;
  `};
`;

const HeaderLeft = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  flex: 1;
  min-width: 200px;
`;

const HeaderRight = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
  flex: 1;
  min-width: 200px;
`;

const Logo = styled.img`
  width: 36px;
`;

const NotificationButton = styled.img`
  width: 36px;
  cursor: pointer;
`;

const StyledLink = styled(Link)`
  color: ${props => props.theme.lightTextColor};
`;

const LeftArrow = styled.i`
  border: solid ${props => props.theme.lightTextColor};
  border-width: 0 3px 3px 0;
  display: inline-block;
  padding: 3px;
  margin-left: 12px;
  margin-right: 6px;
  transform: rotate(135deg);
`;
