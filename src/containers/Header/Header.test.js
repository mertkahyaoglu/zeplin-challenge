import React from 'react';
import renderer from 'react-test-renderer';
import configureStore from 'redux-mock-store';

import TestProviders from 'utils/testProviders';
import Header from './Header';

const mockStore = configureStore([]);

describe('containers/Header', () => {
  it('should render the container', () => {
    const store = mockStore({
      layout: {
        title: 'Test',
      },
    });
    const item = renderer.create(<Header store={store} />).toJSON();

    expect(item).toMatchSnapshot();
  });

  it('should render the container with back button', () => {
    const store = mockStore({
      layout: {
        title: 'Test',
        prevRoute: '/dashboard',
        prevTitle: 'Dashboard',
      },
    });
    const item = renderer
      .create(
        <TestProviders>
          <Header store={store} />
        </TestProviders>
      )
      .toJSON();

    expect(item).toMatchSnapshot();
  });
});
