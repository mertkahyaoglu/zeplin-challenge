import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Screen from 'components/Screen';

const ScreenList = ({ screens, onScreenDoubleClick }) => {
  return (
    <Container>
      {screens.map(screen => (
        <Screen
          key={screen._id}
          name={screen.name}
          thumbnail={screen.thumbnail}
          onDoubleClick={() => onScreenDoubleClick(screen._id)}
        />
      ))}
    </Container>
  );
};

ScreenList.propTypes = {
  screens: PropTypes.array.isRequired,
  onScreenDoubleClick: PropTypes.func.isRequired,
};

const mapStateToProps = ({ filters: { selectedTag } }, { screens }) => ({
  screens: screens.filter(
    screen => (selectedTag ? screen.tags.indexOf(selectedTag) !== -1 : true)
  ),
});

export default connect(mapStateToProps)(ScreenList);

const Container = styled.div`
  display: grid;
  grid-template-rows: auto;
  grid-template-columns: repeat(auto-fill, 220px);
  grid-gap: 24px 22px;
  justify-content: center;
  padding: 0 12px;
  margin-bottom: 12px;
`;
