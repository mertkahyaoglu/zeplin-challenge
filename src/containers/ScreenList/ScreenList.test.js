import React from 'react';
import renderer from 'react-test-renderer';
import configureStore from 'redux-mock-store';

import ScreenList from './ScreenList';

const mockStore = configureStore([]);

const screens = [
  {
    _id: '1',
    name: 'Album',
    tags: ['Music', 'Lock Screen'],
    thumbnail: {
      d1x: '/static/13/screen-thumbnails/1.png',
      d2x: '/static/13/screen-thumbnails/1@2x.png',
    },
  },
  {
    _id: '2',
    name: 'Control Center',
    tags: ['Springboard'],
    thumbnail: {
      d1x: '/static/13/screen-thumbnails/2.png',
      d2x: '/static/13/screen-thumbnails/2@2x.png',
    },
  },
];

describe('containers/ScreenList', () => {
  it('should render the container', () => {
    const store = mockStore({
      filters: {
        selectedTag: null,
      },
    });

    const item = renderer
      .create(
        <ScreenList
          screens={screens}
          onScreenDoubleClick={f => f}
          store={store}
        />
      )
      .toJSON();

    expect(item).toMatchSnapshot();
  });

  it('should render selected tag screen', () => {
    const store = mockStore({
      filters: {
        selectedTag: 'Music',
      },
    });

    const item = renderer
      .create(
        <ScreenList
          screens={screens}
          onScreenDoubleClick={f => f}
          store={store}
        />
      )
      .toJSON();

    expect(item).toMatchSnapshot();
  });
});
