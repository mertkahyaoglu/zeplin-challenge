import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import Resizable from 're-resizable';

import { getProjectUsers } from 'ducks/users';

import Member from 'components/Member';

export class Sidebar extends Component {
  componentDidMount() {
    const { getProjectUsers, match: { params: { pid } } } = this.props;
    getProjectUsers(pid);
  }

  render() {
    const { project, users, loading, error } = this.props;

    if (!users || loading) {
      return null;
    }

    if (error) {
      return error.message;
    }

    return (
      <Container
        handleStyles={{ left: { left: 0, width: 12 } }}
        minWidth={220}
        maxWidth="50%"
        enable={{ left: true }}
      >
        <ScrollHandle>
          <DotWrapper>
            <Dot />
            <Dot />
            <Dot />
          </DotWrapper>
        </ScrollHandle>
        <Scrollable>
          <ProjectInfoWrapper>
            <ProjectTitle>{project.name}</ProjectTitle>
            <ValueWithTitle>
              <Title>Type</Title>
              <Value>{project.type}</Value>
            </ValueWithTitle>
            <ValueWithTitle>
              <Title>Density</Title>
              <Value>{project.density}</Value>
            </ValueWithTitle>
          </ProjectInfoWrapper>
          <MembersWrapper>
            <MembersTitle>
              Members
              <MemberCount>{users.length}</MemberCount>
            </MembersTitle>
            {users.map((user, index) => (
              <Member index={index} key={user.user.username} member={user} />
            ))}
          </MembersWrapper>
        </Scrollable>
      </Container>
    );
  }
}

Sidebar.propTypes = {
  project: PropTypes.object.isRequired,
  loading: PropTypes.bool.isRequired,
  getProjectUsers: PropTypes.func.isRequired,
  users: PropTypes.array,
  error: PropTypes.object,
};

const mapStateToProps = ({ users: { users, loading, error } }) => ({
  users,
  loading,
  error,
});

const mapDispatchToProps = dispatch => ({
  getProjectUsers: pid => dispatch(getProjectUsers(pid)),
});

export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps)
)(Sidebar);

const Container = styled(Resizable)`
  background-color: ${props => props.theme.sidebarColor};
  padding-left: 12px;
`;

const Scrollable = styled.div`
  height: calc(100vh - ${props => props.theme.headerHeight}px);
  overflow: auto;
`;

const ProjectInfoWrapper = styled.div``;
const MembersWrapper = styled.div``;

const ProjectTitle = styled.h2`
  padding: 18px 12px 17px;
  font-size: 18px;
`;

const MembersTitle = styled.h2`
  background-color: ${props => props.theme.sidebarColor};
  padding: 18px 12px 17px;
  font-size: 18px;
  position: sticky;
  top: 0;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const MemberCount = styled.div`
  width: 24px;
  height: 24px;
  background-color: ${props => props.theme.lightOrange};
  border-radius: 50%;
  font-size: 14px;
  color: white;
  text-align: center;
  line-height: 23px;
  font-weight: 300;
`;

const ValueWithTitle = styled.div`
  display: flex;
  padding: 12px;
`;

const Title = styled.div`
  flex: 1;
  color: ${props => props.theme.lightTextColor};
`;

const Value = styled.div`
  flex: 2;
`;

const ScrollHandle = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  background-color: ${props => props.theme.sidebarColor};
  top: 0;
  bottom: 0;
  left: 0;
  width: 12px;
`;

const DotWrapper = styled.div``;

const Dot = styled.div`
  width: 4px;
  height: 4px;
  border-radius: 50%;
  background-color: ${props => props.theme.darkGray};
  margin-bottom: 6px;
`;
