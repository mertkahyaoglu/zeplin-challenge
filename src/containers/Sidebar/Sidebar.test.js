import React from 'react';
import renderer from 'react-test-renderer';
import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';

import TestProviders from 'utils/testProviders';
import Sidebar from './Sidebar';

const mockStore = configureStore([thunk]);

const users = [
  {
    role: 'owner',
    user: {
      username: 'berk',
      email: 'berk@zeplin.io',
      avatar:
        'https://www.gravatar.com/avatar/51b3b8f583a4a342499ecf3d702baf89',
    },
  },
  {
    role: 'user',
    user: {
      username: 'yigit',
      email: 'yigit@zeplin.io',
      avatar:
        'https://www.gravatar.com/avatar/732c4e1146e1b1335fd863029665e0e3',
    },
  },
];

const project = {
  _id: '13',
  name: "Meng's Template ",
  type: 'ios',
  updated: '2017-07-26T12:36:19.988Z',
  created: '2015-09-03T08:54:41.000Z',
  density: '2x',
  screens: [],
};

jest.mock('re-resizable', () => {
  return () => <div>resizable</div>;
});

describe('containers/Sidebar', () => {
  it('should render the container', () => {
    const store = mockStore({
      users: {
        users,
        loading: false,
        error: null,
      },
    });

    const item = renderer
      .create(
        <TestProviders>
          <Sidebar project={project} store={store} />
        </TestProviders>
      )
      .toJSON();

    expect(item).toMatchSnapshot();
  });
});
