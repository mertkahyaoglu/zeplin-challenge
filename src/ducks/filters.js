export const FILTER_SCREEN = 'filters/FILTER_SCREEN';
export const REMOVE_FILTER = 'filters/REMOVE_FILTER';

const initialState = {
  selectedTag: null,
};

export default function reducer(state = initialState, { type, payload }) {
  switch (type) {
    case FILTER_SCREEN:
      return {
        ...state,
        selectedTag: payload,
      };
    case REMOVE_FILTER:
      return {
        ...state,
        selectedTag: null,
      };
    default:
      return state;
  }
}

export const filterScreen = tag => ({
  type: FILTER_SCREEN,
  payload: tag,
});

export const removeFilter = () => ({
  type: REMOVE_FILTER,
});
