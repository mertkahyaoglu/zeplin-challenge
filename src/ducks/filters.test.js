import reducer, {
  FILTER_SCREEN,
  REMOVE_FILTER,
  filterScreen,
  removeFilter,
} from './filters';

describe('Filters actions', () => {
  it('should create an action to add a filter', () => {
    const expectedAction = {
      type: FILTER_SCREEN,
      payload: 'music',
    };
    expect(filterScreen('music')).toEqual(expectedAction);
  });
  it('should create an action to remove the filter', () => {
    const expectedAction = {
      type: REMOVE_FILTER,
    };
    expect(removeFilter()).toEqual(expectedAction);
  });
});

describe('Filters reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      selectedTag: null,
    });
  });

  it('should handle FILTER_SCREEN', () => {
    expect(
      reducer(
        {
          selectedTag: null,
        },
        {
          type: FILTER_SCREEN,
          payload: 'music',
        }
      )
    ).toEqual({
      selectedTag: 'music',
    });
  });

  it('should handle REMOVE_FILTER', () => {
    expect(
      reducer(
        {
          selectedTag: 'music',
        },
        {
          type: REMOVE_FILTER,
        }
      )
    ).toEqual({
      selectedTag: null,
    });
  });
});
