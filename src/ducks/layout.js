export const UPDATE_HEADER = 'layout/UPDATE_HEADER';

const initialState = {
  title: null,
  prevRoute: null,
  prevTitle: null,
};

export default function reducer(state = initialState, { type, payload }) {
  switch (type) {
    case UPDATE_HEADER:
      return {
        ...state,
        ...payload,
      };
    default:
      return state;
  }
}

export const updateHeader = (title, prevRoute, prevTitle) => ({
  type: UPDATE_HEADER,
  payload: { title, prevRoute, prevTitle },
});
