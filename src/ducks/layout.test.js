import reducer, { UPDATE_HEADER, updateHeader } from './layout';

describe('Layout actions', () => {
  it('should create an action to update the header', () => {
    const expectedAction = {
      type: UPDATE_HEADER,
      payload: {
        title: 'Home',
        prevRoute: '/dashboard',
        prevTitle: 'Dashboard',
      },
    };
    expect(updateHeader('Home', '/dashboard', 'Dashboard')).toEqual(
      expectedAction
    );
  });
});

describe('Layout reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      title: null,
      prevTitle: null,
      prevRoute: null,
    });
  });

  it('should handle UPDATE_HEADER', () => {
    expect(
      reducer(
        {
          title: null,
          prevTitle: null,
          prevRoute: null,
        },
        {
          type: UPDATE_HEADER,
          payload: {
            title: 'Home',
            prevRoute: '/dashboard',
            prevTitle: 'Dashboard',
          },
        }
      )
    ).toEqual({
      title: 'Home',
      prevRoute: '/dashboard',
      prevTitle: 'Dashboard',
    });
  });
});
