import { getProjectUrl } from 'config/api';

export const PROJECT_REQUEST = 'project/REQUEST';
export const PROJECT_SUCCESS = 'project/SUCCESS';
export const PROJECT_ERROR = 'project/ERROR';

const initialState = {
  loading: false,
  error: null,
  project: null,
};

export default function reducer(state = initialState, { type, payload }) {
  switch (type) {
    case PROJECT_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case PROJECT_ERROR:
      return {
        ...state,
        error: payload,
        loading: false,
      };
    case PROJECT_SUCCESS:
      return {
        ...state,
        project: payload,
        loading: false,
      };
    default:
      return state;
  }
}

export const getProject = pid => async dispatch => {
  const url = getProjectUrl(pid);

  try {
    dispatch({ type: PROJECT_REQUEST });
    const response = await fetch(url);
    const data = await response.json();
    if (response.ok) {
      dispatch({ type: PROJECT_SUCCESS, payload: data });
    } else {
      dispatch({ type: PROJECT_ERROR, payload: data });
    }
  } catch (error) {
    dispatch({ type: PROJECT_ERROR, payload: { message: error } });
  }
};
