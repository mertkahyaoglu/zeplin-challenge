import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';

import { getProjectUrl } from 'config/api';
import reducer, {
  PROJECT_REQUEST,
  PROJECT_SUCCESS,
  PROJECT_ERROR,
  getProject,
} from './project';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('Project actions', () => {
  afterEach(() => {
    fetchMock.reset();
    fetchMock.restore();
  });

  it('creates PROJECT_SUCCESS when fetching project has been done', () => {
    const id = 13;

    fetchMock.getOnce(getProjectUrl(id), [mockContributor]);
    const expectedActions = [
      { type: PROJECT_REQUEST },
      { type: PROJECT_SUCCESS, payload: [mockContributor] },
    ];
    const store = mockStore({ contibutors: {} });

    return store.dispatch(getProject(id)).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});

describe('Project reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      loading: false,
      error: null,
      project: null,
    });
  });

  it('should handle PROJECT_REQUEST', () => {
    expect(
      reducer(
        {
          loading: false,
          project: null,
          error: null,
        },
        {
          type: PROJECT_REQUEST,
        }
      )
    ).toEqual({
      loading: true,
      project: null,
      error: null,
    });
  });

  it('should handle PROJECT_ERROR', () => {
    expect(
      reducer(
        { loading: true, project: null, error: null },
        {
          type: PROJECT_ERROR,
          payload: 'error',
        }
      )
    ).toEqual({
      loading: false,
      project: null,
      error: 'error',
    });
  });

  it('should handle PROJECT_SUCCESS', () => {
    expect(
      reducer(
        { loading: true, project: null, error: null },
        {
          type: PROJECT_SUCCESS,
          payload: [{ login: 'mert' }],
        }
      )
    ).toEqual({
      loading: false,
      project: [{ login: 'mert' }],
      error: null,
    });
  });
});

const mockContributor = {
  _id: '13',
  name: "Meng's Template ",
  type: 'ios',
  updated: '2017-07-26T12:36:19.988Z',
  created: '2015-09-03T08:54:41.000Z',
  density: '2x',
  screens: [
    {
      _id: '1',
      name: 'Album',
      tags: ['Music', 'Lock Screen'],
      thumbnail: {
        d1x: '/static/13/screen-thumbnails/1.png',
        d2x: '/static/13/screen-thumbnails/1@2x.png',
      },
    },
    {
      _id: '2',
      name: 'Control Center',
      tags: ['Springboard'],
      thumbnail: {
        d1x: '/static/13/screen-thumbnails/2.png',
        d2x: '/static/13/screen-thumbnails/2@2x.png',
      },
    },
    {
      _id: '3',
      name: 'Home',
      tags: ['Springboard'],
      thumbnail: {
        d1x: '/static/13/screen-thumbnails/3.png',
        d2x: '/static/13/screen-thumbnails/3@2x.png',
      },
    },
  ],
};
