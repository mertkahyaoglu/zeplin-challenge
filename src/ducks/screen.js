import { getProjectScreenUrl } from 'config/api';

export const SCREEN_REQUEST = 'screen/REQUEST';
export const SCREEN_SUCCESS = 'screen/SUCCESS';
export const SCREEN_ERROR = 'screen/ERROR';

const initialState = {
  loading: false,
  error: null,
  screen: null,
};

export default function reducer(state = initialState, { type, payload }) {
  switch (type) {
    case SCREEN_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case SCREEN_ERROR:
      return {
        ...state,
        error: payload,
        loading: false,
      };
    case SCREEN_SUCCESS:
      return {
        ...state,
        screen: payload,
        loading: false,
      };
    default:
      return state;
  }
}

export const getProjectScreen = (pid, sid) => async dispatch => {
  const url = getProjectScreenUrl(pid, sid);

  try {
    dispatch({ type: SCREEN_REQUEST });
    const response = await fetch(url);
    const data = await response.json();

    if (response.ok) {
      dispatch({ type: SCREEN_SUCCESS, payload: data });
    } else {
      dispatch({ type: SCREEN_ERROR, payload: data });
    }
  } catch (error) {
    dispatch({ type: SCREEN_ERROR, payload: { message: error } });
  }
};
