import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';

import { getProjectScreenUrl } from 'config/api';
import reducer, {
  SCREEN_REQUEST,
  SCREEN_SUCCESS,
  SCREEN_ERROR,
  getProjectScreen,
} from './screen';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('Screen actions', () => {
  afterEach(() => {
    fetchMock.reset();
    fetchMock.restore();
  });

  it('creates SCREEN_SUCCESS when fetching screen has been done', () => {
    const pid = 13;
    const sid = 1;

    fetchMock.getOnce(getProjectScreenUrl(pid, sid), [mockContributor]);
    const expectedActions = [
      { type: SCREEN_REQUEST },
      { type: SCREEN_SUCCESS, payload: [mockContributor] },
    ];
    const store = mockStore({ contibutors: [] });

    return store.dispatch(getProjectScreen(pid, sid)).then(() => {
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});

describe('Screen reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      screen: null,
      loading: false,
      error: null,
    });
  });

  it('should handle SCREEN_REQUEST', () => {
    expect(
      reducer(
        {
          screen: null,
          loading: false,
          error: null,
        },
        {
          type: SCREEN_REQUEST,
        }
      )
    ).toEqual({
      screen: null,
      loading: true,
      error: null,
    });
  });

  it('should handle SCREEN_ERROR', () => {
    expect(
      reducer(
        { loading: true, screen: null, error: null },
        {
          type: SCREEN_ERROR,
          payload: 'error',
        }
      )
    ).toEqual({
      screen: null,
      loading: false,
      error: 'error',
    });
  });

  it('should handle SCREEN_SUCCESS', () => {
    expect(
      reducer(
        { loading: true, screen: null, error: null },
        {
          type: SCREEN_SUCCESS,
          payload: { name: 'Album' },
        }
      )
    ).toEqual({
      loading: false,
      screen: { name: 'Album' },
      error: null,
    });
  });
});

const mockContributor = {
  _id: '1',
  name: 'Album',
  tags: ['Music', 'Lock Screen'],
  thumbnail: {
    d1x: '/static/13/screen-thumbnails/1.png',
    d2x: '/static/13/screen-thumbnails/1@2x.png',
  },
  width: 750,
  height: 1334,
  imageUrl: '/static/13/screen-images/1.png',
  backgroundColor: {
    r: 255,
    b: 255,
    g: 255,
    a: 1,
  },
};
