import { getProjectUsersUrl } from 'config/api';
import roles from 'config/roles';

export const USERS_REQUEST = 'users/REQUEST';
export const USERS_SUCCESS = 'users/SUCCESS';
export const USERS_ERROR = 'users/ERROR';

const initialState = {
  loading: false,
  error: null,
  users: [],
};

export default function reducer(state = initialState, { type, payload }) {
  switch (type) {
    case USERS_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case USERS_ERROR:
      return {
        ...state,
        error: payload,
        loading: false,
      };
    case USERS_SUCCESS:
      return {
        ...state,
        users: payload.users.sort(
          (a, b) =>
            roles.indexOf(a.role) - roles.indexOf(b.role) ||
            a.user.username > b.user.username
        ),
        loading: false,
      };
    default:
      return state;
  }
}

export const getProjectUsers = pid => async dispatch => {
  const url = getProjectUsersUrl(pid);

  try {
    dispatch({ type: USERS_REQUEST });
    const response = await fetch(url);
    const data = await response.json();
    if (response.ok) {
      dispatch({ type: USERS_SUCCESS, payload: data });
    } else {
      dispatch({ type: USERS_ERROR, payload: data });
    }
  } catch (error) {
    dispatch({ type: USERS_ERROR, payload: error });
  }
};
