import React from 'react';
import ReactDOM from 'react-dom';
import { injectGlobal, ThemeProvider } from 'styled-components';
import { normalize } from 'polished';
import reset from 'styled-reset';
import { Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import { createBrowserHistory } from 'history';
import store from './store';
import App from './App';
import theme from './utils/theme';

const history = createBrowserHistory();

/* eslint-disable no-unused-expressions*/
injectGlobal`${normalize()}`;
injectGlobal`${reset}`;
injectGlobal`
  html {
    font-family: 'Roboto', Helvetica, Arial, sans-serif;
    font-size: 15px;
    -webkit-font-smoothing: antialiased;
    background-color: white;
  }

  a {
    text-decoration: none;
  }
`;

ReactDOM.render(
  <Provider store={store}>
    <ThemeProvider theme={theme}>
      <Router history={history}>
        <App />
      </Router>
    </ThemeProvider>
  </Provider>,
  document.getElementById('root')
);
