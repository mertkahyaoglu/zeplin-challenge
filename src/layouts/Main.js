import React, { Fragment } from 'react';
import styled from 'styled-components';
import { Redirect, Route, Switch } from 'react-router-dom';

import Header from 'containers/Header';
import { Dashboard, ScreenDetail, NotFound } from 'pages';

const Main = () => {
  return (
    <Fragment>
      <Header />
      <MainWrapper>
        <Switch>
          <Route exact component={Dashboard} path="/project/:pid" />
          <Route
            exact
            component={ScreenDetail}
            path="/project/:pid/screen/:sid"
          />
          <Redirect exact from="/" to="/project/13" /* For test purposes */ />
          <Route component={NotFound} />
        </Switch>
      </MainWrapper>
    </Fragment>
  );
};

export default Main;

const MainWrapper = styled.div`
  display: flex;
  height: calc(100vh - ${props => props.theme.headerHeight}px);
`;
