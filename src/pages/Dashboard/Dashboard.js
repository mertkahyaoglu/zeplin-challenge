import React, { Component, Fragment } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { updateHeader } from 'ducks/layout';
import { getProject } from 'ducks/project';

import Loading from 'components/Loading';
import ErrorMessage from 'components/ErrorMessage';
import Sidebar from 'containers/Sidebar';
import Filters from 'containers/Filters';
import ScreenList from 'containers/ScreenList';

export class Dashboard extends Component {
  componentWillReceiveProps({ project, updateHeader }) {
    if (project) {
      updateHeader(project.name, null, null);
    }
  }

  componentDidMount() {
    const { getProject, match: { params: { pid } } } = this.props;
    getProject(pid);
  }

  handleScreenDoubleClick = sid => {
    const { history, match: { params: { pid } } } = this.props;
    history.push(`/project/${pid}/screen/${sid}`);
  };

  render() {
    const { project, loading, error } = this.props;

    if (error) {
      return <ErrorMessage error={error.message} />;
    }

    if (!project || loading) {
      return <Loading />;
    }

    const allTags = project.screens.reduce(
      (acc, curr) => acc.concat(curr.tags),
      []
    );
    const uniqueTags = Array.from(new Set(allTags)).sort();

    return (
      <Fragment>
        <Content>
          <Filters tags={uniqueTags} />
          <ScreenList
            screens={project.screens}
            onScreenDoubleClick={this.handleScreenDoubleClick}
          />
        </Content>
        <Sidebar project={project} />
      </Fragment>
    );
  }
}

Dashboard.propTypes = {
  loading: PropTypes.bool.isRequired,
  getProject: PropTypes.func.isRequired,
  project: PropTypes.object,
  error: PropTypes.object,
};

const mapStateToProps = ({ project: { project, loading, error } }) => ({
  project,
  loading,
  error,
});

const mapDispatchToProps = dispatch => ({
  getProject: pid => dispatch(getProject(pid)),
  updateHeader: (title, prevRoute, prevTitle) =>
    dispatch(updateHeader(title, prevRoute, prevTitle)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);

const Content = styled.div`
  position: relative;
  flex: 1;
  height: 100%;
  overflow: auto;
`;
