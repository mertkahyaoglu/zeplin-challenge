import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

const NotFound = () => {
  return (
    <Container>
      Sorry! This page does not exist. <Link to="/"> Go to homepage!</Link>
    </Container>
  );
};

export default NotFound;

const Container = styled.div`
  display: flex;
  flex: 1;
  justify-content: center;
  align-items: center;
  font-size: 18px;
  font-weight: 500;
`;
