import React, { Component } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { getProjectScreen } from 'ducks/screen';
import { updateHeader } from 'ducks/layout';

import Loading from 'components/Loading';
import ErrorMessage from 'components/ErrorMessage';

import { getAssetUrl } from 'utils/helpers';

export class ScreenDetail extends Component {
  componentWillReceiveProps({
    screen,
    updateHeader,
    match: { params: { pid } },
  }) {
    if (screen) {
      updateHeader(screen.name, `/project/${pid}`, 'Dashboard');
    }
  }

  componentDidMount() {
    const { getProjectScreen, match: { params: { pid, sid } } } = this.props;
    getProjectScreen(pid, sid);
  }

  render() {
    const { screen, loading, error } = this.props;

    if (error) {
      return <ErrorMessage error={error.message} />;
    }

    if (!screen || loading) {
      return <Loading />;
    }

    return (
      <Viewport>
        <Image
          src={getAssetUrl(screen.imageUrl)}
          width={screen.width}
          height={screen.height}
        />
      </Viewport>
    );
  }
}

ScreenDetail.propTypes = {
  loading: PropTypes.bool.isRequired,
  getProjectScreen: PropTypes.func.isRequired,
  screen: PropTypes.object,
  error: PropTypes.object,
};

const mapStateToProps = ({ screen: { screen, loading, error } }) => ({
  screen,
  loading,
  error,
});

const mapDispatchToProps = dispatch => ({
  getProjectScreen: (pid, sid) => dispatch(getProjectScreen(pid, sid)),
  updateHeader: (title, prevRoute, prevTitle) =>
    dispatch(updateHeader(title, prevRoute, prevTitle)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ScreenDetail);

const Viewport = styled.div`
  width: 100%;
  height: calc(100vh - ${props => props.theme.headerHeight}px);
  background-color: ${props => props.theme.primaryColor};
  display: flex;
  justify-content: center;
  overflow: auto;
`;

const Image = styled.img`
  padding-top: 36px;
`;
