import React from 'react';
import Loadable from 'react-loadable';

const Loader = opts =>
  Loadable({
    loading: () => <div>...</div>,
    delay: 200,
    timeout: 10,
    ...opts,
  });

export const Dashboard = Loader({
  loader: () => import('./Dashboard'),
});

export const ScreenDetail = Loader({
  loader: () => import('./ScreenDetail'),
});

export const NotFound = Loader({
  loader: () => import('./NotFound'),
});
