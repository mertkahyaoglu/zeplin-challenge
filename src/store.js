import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import * as reducers from './ducks';

const store = createStore(
  combineReducers(reducers),
  compose(
    applyMiddleware(thunk),
    typeof global.__REDUX_DEVTOOLS_EXTENSION__ !== 'undefined'
      ? global.__REDUX_DEVTOOLS_EXTENSION__()
      : f => f
  )
);

export default store;
