import { css } from 'styled-components';
import { BASE_URL } from 'config/api';
import theme from 'utils/theme';

export const media = Object.keys(theme).reduce((accumulator, label) => {
  const size = theme[label];
  /* eslint-disable no-param-reassign*/
  accumulator[label] = (...args) => css`
    @media (max-width: ${size}px) {
      ${css(...args)};
    }
  `;
  /* eslint-enable no-param-reassign*/
  return accumulator;
}, {});

export const getAssetUrl = path => BASE_URL + path;
